angular
.module('app')
.controller('loginCtrl', loginCtrl);

loginCtrl.$inject = ['$scope', '$resource', '$state'];
function loginCtrl($scope, $resource, $state) {	

	//variables
	$scope.error = false;
	$scope.email = '';
	$scope.pwd = '';

	//methods
	$scope.login = function() {

		var data = {
			email: $scope.email,
			password: $scope.pwd
		};
		
		var User = $resource('http://23.253.107.65/api/v2/user/session/');
		User.save(data).$promise
			.then(function(response) {
				$state.go('app.main');
			},
			function(error) {
				$scope.error = true;
				console.log("Error message", error);
			});
	}
};
